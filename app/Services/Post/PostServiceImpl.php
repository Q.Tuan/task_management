<?php

namespace App\Services\Post;

use App\Http\Repositories\Post\PostRepositoryInterface;
use App\Models\Hashtag;
use App\Models\Post;
use App\Services\MainService;
use Illuminate\Support\Facades\Cache;

class PostServiceImpl extends MainService implements PostServiceInterface
{
    /**
     * get model
     * @return string
     */
    public function getRepository()
    {
        return PostRepositoryInterface::class;
    }

    /**
     * Create new post
     * @param array<string, string> $data
     * @return mixed
     */
    public function create(array $data)
    {
        $user = auth()->user();
        if (!$user) {
            return;
        }

        $data['view_number'] = 0;
        $data['user_id'] =  $user->id;

        // Create the post
        $post = $this->_repository->create($data);

        if (isset($data['hashtags'])) {
            $hashtags = array_filter(array_map(function ($tag) {
                return str_replace('#', '', trim($tag));
            }, explode(',', $data['hashtags'])));
            foreach ($hashtags as $hashtagText) {
                $hashtag = Hashtag::firstOrCreate(['name' => $hashtagText]);
                $post->hashtags()->attach($hashtag->id);
            }
        }

        return $post;
    }

    /**
     * @param array<string, string> $data
     * @return mixed
     */
    public function update($id, array $data)
    {
        $post = Post::findOrFail($id);
        $user = isset(auth()->user()->id) ? auth()->user() : null;
        if ($user && $post->user_id !== $user->id && !$user->hasRole('admin')) {
            throw new \Exception('Unauthorized access.'); // Throw a generic Exception
        }
 

        // Detach existing hashtags
        $post->hashtags()->detach();

        // Update or create new hashtags
        if (isset($data['hashtags'])) {
            $hashtags = array_filter(array_map(function ($tag) {
                return str_replace('#', '', trim($tag));
            }, explode(',', $data['hashtags'])));
        }

        unset($data['hashtags']);

        // Update post attributes
        $this->_repository->update($id, $data);

        if ($hashtags) {
            foreach ($hashtags as $hashtagText) {
                $hashtag = Hashtag::firstOrCreate(['name' => $hashtagText]);
                $post->hashtags()->attach($hashtag->id);
            }
        }

        return $post;
    }

    /**
     * Get all posts or by author with pagination
     * @return  \Illuminate\Pagination\LengthAwarePaginator<\App\Models\Post>
     */
    public function get(string $author = "all", int $page = 1)
    {
        if ($author == 'all') {
            $cacheKey = 'posts_page_' . $page;
            $posts = Cache::remember($cacheKey, 60, function () use ($author, $page) {
                return $this->_repository->get($author, $page);
            });
        } else {
            $cacheKey = "posts_{$author}_page_{$page}";
            $posts = Cache::remember($cacheKey, 60, function () use ($author, $page) {
                return $this->_repository->get($author, $page);
            });
        }
        return $posts;
    }



    /**
     * Get post by id
     * @return Post
     */
    public function getById(int $id)
    {
        return Post::with('user', 'hashtags')->findOrFail($id);
    }

    /**
     * Get New Posts
     * @return  \Illuminate\Pagination\LengthAwarePaginator<\App\Models\Post>
     */
    public function getNewPosts()
    {
        return $this->_repository->getNewPosts();
    }

    /**
     * Get Most views Posts
     * @return  \Illuminate\Pagination\LengthAwarePaginator<\App\Models\Post>
     */
    public function getMostViewPosts()
    {
        return $this->_repository->getMostViewPosts();
    }
}
