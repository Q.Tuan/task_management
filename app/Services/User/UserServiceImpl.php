<?php

namespace App\Services\User;

use App\Http\Repositories\User\UserRepositoryInterface;
use App\Jobs\SendMailActiveUser;
use App\Models\Role;
use App\Models\User;
use App\Services\MainService;
use Illuminate\Support\Arr;

class UserServiceImpl extends MainService implements UserServiceInterface
{
    /**
     * get model
     * @return string
     */
    public function getRepository()
    {
        return UserRepositoryInterface::class;
    }


    /**
     * Create a new user.
     *
     * @param array<string, string>  $request
     * @return \App\Models\User
     */
    public function create(array $request): User
    {
        // Encrypt the password before storing
        $request['password'] = bcrypt($request['password']);

        $user = $this->_repository->create($request);

        $userRole = Role::where('name', 'user')->first();
        $user->roles()->attach($userRole);

        // event(new Registered($user));

        SendMailActiveUser::dispatch($user)->onQueue('email');


        return $user;
    }


    /**
     * Update an existing user.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @return \App\Models\User
     */
    public function update($id, $request): User
    {
        // Validate the request data
        $validatedData = $request->validated();

        $userDb = $this->_repository->update($id, Arr::except($validatedData, ['avatar, password']));

        if (!empty($validatedData['avatar'])) {
            $userDb->avatar = $validatedData['avatar']; // Assuming you're updating the avatar path
        }
        // Update the password only if provided
        if ($request->filled('password')) {
            $request->validate([
                'password' => 'required|string|min:8|confirmed'
            ]);
            $userDb->password = bcrypt($request['password']); // Hash the new password
        }

        $userDb->save();
        return $userDb;
    }
}
